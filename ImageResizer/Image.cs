﻿using System;
using System.Drawing;

namespace ImageResizer
{
    public class Image
    {
        public string ImagePath { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        private Bitmap ImageFile { get; set; }
    
        public Image(string path)
        {
            ImagePath = path;
            ImageFile = new Bitmap(path);
            ImageWidth = ImageFile.Width;
            ImageHeight = ImageFile.Height;
        }

        public Bitmap Resize(int newWidth, int newHeight)
        {
            var resizedImage = new Bitmap(newWidth, newHeight);

            var ratioX = (double)ImageWidth / (double)newWidth;
            var ratioY = (double)ImageHeight / (double)newHeight;
            
            for (var i = 0; i < newWidth-1; i++)
            {
                for (var j = 0; j < newHeight-1; j++)
                {
                    var sourceX = (int)Math.Round(i * ratioX);
                    var sourceY = (int)Math.Round(j * ratioY);

                    if (sourceY == ImageHeight)
                        sourceY -= 1;
                    if (sourceX == ImageWidth)
                        sourceX -= 1;

                    var pixel = ImageFile.GetPixel(sourceX, sourceY);

                    resizedImage.SetPixel(i, j, pixel);
                }
            }
            return resizedImage;
        }
    }
}
