﻿using System.Windows;
using Microsoft.Win32;

namespace ImageResizer
{
    public partial class MainWindow : Window
    {
        public Image ImageFile { get; set; }
        public MainWindow()
        {
            InitializeComponent();            
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            var browseWindow = new OpenFileDialog
            {
                Filter = "Image (*.jpg, *.jpeg, *.png, *.bmp) | *.jpg; *.jpeg; *.png; *.bmp | All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };


            if (browseWindow.ShowDialog() != true) return;
            ImageFile = new Image(browseWindow.FileName);
            tbPath.Text = browseWindow.FileName;
            txtResH.Content = "H: " + ImageFile.ImageHeight.ToString();
            txtResW.Content = "W: " + ImageFile.ImageWidth.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            btnResize.IsEnabled = false;
            if (ImageFile == null)
            {
                MessageBox.Show("Choose image to resize");
                return;
            }
            if (txtNewW.Text == "" || txtNewH.Text == "")
            {
                MessageBox.Show("Setup new resolution");
                return;
            }

            var resizedImage = ImageFile.Resize(int.Parse(txtNewW.Text), int.Parse(txtNewH.Text));
            resizedImage.Save(string.Format("{0}_{1}x{2}.jpg", ImageFile.ImagePath.Substring(0, ImageFile.ImagePath.Length - 4), txtNewW.Text, txtNewH.Text));
            btnResize.IsEnabled = true;
        }
    }
}
